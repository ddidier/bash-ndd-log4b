#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=1090
source "${PROJECT_DIR}/lib/ansi/ansi"

function main() {

  local archive_version
  local distribution_dir="${PROJECT_DIR}/dist"

  if [[ -z "${1+x}" ]]; then
    archive_version="latest"
  else
    archive_version="${1}"
  fi

  echo "Packaging distribution to '${distribution_dir}/ndd-log4b-${archive_version}.tar.gz'"

  rm -rf "${distribution_dir:?}/"

  # ---------- ndd-log4b

  mkdir -p "${distribution_dir}/ndd-log4b-${archive_version}/"

  cp "${PROJECT_DIR}/src/ndd-log4b.sh" "${distribution_dir}/ndd-log4b-${archive_version}/ndd-log4b-${archive_version}.sh"

  ln -s "ndd-log4b-${archive_version}" "${distribution_dir}/ndd-log4b"
  ln -s "ndd-log4b-${archive_version}.sh" "${distribution_dir}/ndd-log4b-${archive_version}/ndd-log4b.sh"

  sed -i -E 's/@NDD_LOG4B_VERSION@/'"${archive_version}"'/g' "${distribution_dir}/ndd-log4b-${archive_version}/ndd-log4b-${archive_version}.sh"

  # ---------- dependencies

  local dependency_ansi_path
  local dependency_ansi_name
  dependency_ansi_path="$(realpath "${PROJECT_DIR}/lib/ansi")"
  dependency_ansi_name="$(basename "${dependency_ansi_path}")"

  cp -r "${dependency_ansi_path}" "${distribution_dir}/"

  ln -s "${dependency_ansi_name}" "${distribution_dir}/ansi"

  # ---------- package

  tar cfz "${distribution_dir}/ndd-log4b-${archive_version}.tar.gz" -C "${distribution_dir}/" \
    "ndd-log4b-${archive_version}" "ndd-log4b" \
    "${dependency_ansi_name}" "ansi"

  ansi --bold --green " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  ansi --bold --green " ┃ PACKAGE -- Success!                                                          "
  ansi --bold --green " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    ansi --bold --red " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    ansi --bold --red " ┃ PACKAGE -- Failed!                                                           "
    ansi --bold --red " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

    exit 1
}

trap 'error_handler ${?}' ERR

main "$@"

exit 0
