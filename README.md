
# NDD Log4B

A simple logging library for Bash, featuring:

- two independent appenders: stdout and file
- date format customization
- log format customization
- look and feel customization
- profiles, i.e. collections of log level hierarchy, appender formats, look and feels

<!-- MarkdownTOC -->

1. [Requirements](#requirements)
1. [Installation](#installation)
1. [Usage](#usage)
    1. [Appenders](#appenders)
    1. [Levels](#levels)
    1. [Profiles](#profiles)
1. [Development](#development)
    1. [Installing](#installing)
    1. [Coding](#coding)

<!-- /MarkdownTOC -->



<a id="requirements"></a>
## Requirements

This library has been tested with with Bash 5.x.
You can display your Bash version `bash -c 'echo "${BASH_VERSION}"'`.

This library has an optional dependency on [ANSI] which add colors to messages.



<a id="installation"></a>
## Installation

[Download a release](https://gitlab.com/ddidier/bash-ndd-log4b/-/releases) and extract the files somewhere in your project.
The archive also contains the optional dependency [ANSI] but you may also get this file elsewhere.



<a id="usage"></a>
## Usage

Source `ndd-log4b.sh` and optionaly `ansi` in your script:

```bash
source ansi
source ndd-log4b.sh
```

Logging a message is then as simple as calling: `log error "Some really worrying message"`.

Messages can also be a `printf` construct: `log error "%d really worrying messages" 3`.
In this case, pay extra attention to special characters to `printf` such as `-` (dash):

- `log error "%s %d special character(s) with printf" "-" "1"` will work because the dash is a `printf` argument
- `log error "- %d special character(s) with printf" "1"` won't work because the dash is a special character
- `log error "- 1 special character(s) with printf"` will work because there is only a single argument so `echo` is used under the hood

Continue reading for more details...

<a id="appenders"></a>
### Appenders

This logging library provides two appenders which can be used independently.

**Console appender**

The console appender sends all the messages to `stdout` and is enabled by default.

You can change its configuration by calling some *setters*:

```bash
# The level of the stdout appender
ndd::logger::set_stdout_level "WARNING"
# The date format of the stdout appender ('date' command format)
ndd::logger::set_stdout_date_format "+%F %T"
# The message format of the stdout appender ('printf' command format)
# The accepted arguments are: date-time (%s), level (%s), message (%s)
ndd::logger::set_stdout_log_format "%s [%-5s] %s"
```

For example, the previous configuration produces: `2020-12-31 22:30:55 [WARNING    ] Some severe warning`

**File appender**

The console appender sends all the messages to a given file and is disabled by default.

You can change its configuration by calling some *setters*:

```bash
# The level of the file appender
ndd::logger::set_file_level "DISABLED"
# The date format of the file appender ('date' command format)
ndd::logger::set_file_date_format "+%F %T"
# The message format of the file appender ('printf' command format)
# The accepted arguments are: date-time (%s), level (%s), message (%s)
ndd::logger::set_file_log_format "%s [%-5s] %s"
# The file path of the file appender
ndd::logger::set_file_path "/tmp/some.log"
```

To enable this appender, you must at least change the file level and set the file path.

<a id="levels"></a>
### Levels

Each log level has a name (e.g. `WARNING`), a display name (e.g. `WARN`), a value (e.g. `4`), a "look and feel" (e.g. bold red), and may have one or more synonyms (e.g. `WARN`).

Display names can be set using the associative array `NDD_LOG4B_LEVEL_DISPLAY_NAMES`, the key being the level name and the value the display name, e.g. `NDD_LOG4B_LEVEL_DISPLAY_NAMES["WARNING"]="WARN"`.

Look and feels can be set using the associative array `NDD_LOG4B_LEVEL_ANSI_ARGS`, the key being the level name and the value the arguments passed to the [ansi] command, e.g. `NDD_LOG4B_LEVEL_ANSI_ARGS["ERROR"]="--red"`.

Synonyms can be added using the associative array `NDD_LOG4B_LEVEL_SYNONYMS`, the key being the level synonym and the value the level name, e.g. `NDD_LOG4B_LEVEL_SYNONYMS["WARN"]="WARNING"`.

<a id="profiles"></a>
### Profiles

A profile is a collection of configuration options.
There is 4 profiles at the moment, but you can easily create your own.

**Log4j profile (default)**

This is the default profile.

The default levels are defined following the [Log4j] semantics:

- `fatal`
- `error`
- `warning` (synonym: `warn`)
- `information` (synonym: `info`)
- `debug`
- `trace`

This profile configures both the stdout appender and the file appender.

This profile can be activated by calling the `ndd::logger::profiles::use_log4j` function.

**RFC 5424 profile**

The default levels are defined by the [RFC 5424]:

- `emergency` (synonym: `emerg`)
- `alert`
- `critical` (synonym: `crit`)
- `error`
- `warning` (synonym: `warn`)
- `notice`
- `informational` (synonym: `info`)
- `debug`

This profile configures both the stdout appender and the file appender.

This profile can be activated by calling the `ndd::logger::profiles::use_rfc_5424` function.

**JSON file profile**

This profile configures the file appender to write messages as JSON.
The stdout appender is not modified.

This profile can be activated by calling the `ndd::logger::profiles::use_json_for_file` function.

**JSON stdout profile**

This profile configures the stdout appender to write messages as JSON.
The file appender is not modified.

This profile can be activated by calling the `ndd::logger::profiles::use_json_for_stdout` function.




<a id="development"></a>
## Development

<a id="installing"></a>
### Installing

The development of this library requires the following tools:

- [ShellCheck] for the static analysis of the code
- [Bashcov] for the code coverage (this is a Ruby gem so you should use [RVM])

<a id="coding"></a>
### Coding

The `Makefile` provide some useful targets:

- `make setup` to install the development tools ([RVM] must be installed)
- `make quality` to run the quality checks ([ShellCheck])
- `make test` to run the tests
- `make package` to package the library
- `make` is the default target and is a shortcut to `make quality test`





[ANSI]: https://github.com/fidian/ansi/
[Bashcov]: https://github.com/infertux/bashcov/
[Log4j]: https://logging.apache.org/log4j/2.x/
[RFC 5424]: https://tools.ietf.org/html/rfc5424/
[RVM]: https://rvm.io/
[ShellCheck]: https://github.com/koalaman/shellcheck/
