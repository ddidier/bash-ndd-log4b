
# NDD Log4B

## Version 0.3.0

- Improve performances
- Change the function used to test if 'ansi' is present

**Breaking changes**

The configuration variables have been replaced by *setters* to improve performances:

- `NDD_LOG4B_STDOUT_LEVEL` replaced by `ndd::logger::set_stdout_level`
- `NDD_LOG4B_STDOUT_DATE_FORMAT` replaced by `ndd::logger::set_stdout_date_format`
- `NDD_LOG4B_STDOUT_LOG_FORMAT` replaced by `ndd::logger::set_stdout_log_format`
- `NDD_LOG4B_FILE_LEVEL` replaced by `ndd::logger::set_file_level`
- `NDD_LOG4B_FILE_DATE_FORMAT` replaced by `ndd::logger::set_file_date_format`
- `NDD_LOG4B_FILE_LOG_FORMAT` replaced by `ndd::logger::set_file_level`
- `NDD_LOG4B_FILE_PATH` replaced by `ndd::logger::set_file_path`

## Version 0.2.1

- Check if the terminal supports colors (fix)

## Version 0.2.0

- Check if the terminal supports colors
- Replace `NDD_LOG4B_ENABLE_ANSI` with checks in functions
- Rename `NDD_LOG4B_FILE` to `NDD_LOG4B_FILE_PATH`

## Version 0.1.1

- Add a release process (issue [#1](https://gitlab.com/ddidier/bash-ndd-log4b/-/issues/1))
- Clean utility scripts

## Version 0.1.0

- Initial release
