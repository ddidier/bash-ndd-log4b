#!/usr/bin/env bash



function _replace_default_date() {
  local message="${1}"
  local current_year
  current_year=$(date "+%Y")
  echo "${message}" | sed -E 's/'"${current_year}"'-[[:digit:]]{2}-[[:digit:]]{2} [[:digit:]]{2}:[[:digit:]]{2}:[[:digit:]]{2}/_DATE_/g'
}

function _replace_timestamp() {
  local message="${1}"
  local current_timestamp_prefix
  current_timestamp_prefix=$(date "+%s" | cut -c1-6)
  echo "${message}" | sed -E 's/'"${current_timestamp_prefix}"'[[:digit:]]{4}/_TIMESTAMP_/g'
}

function _strip_escape_codes() {
  local message="${1}"
  # shellcheck disable=SC2001
  echo "${message}" | sed 's/\x1B\[[0-9;]\{1,\}[A-Za-z]//g'
}

function _normalize() {
  local message="${1}"
  message=$(_strip_escape_codes "${message}")
  message=$(_replace_default_date "${message}")
  echo -e "${message}"
}

function _debug_escape_codes() {
  local message="${1}"
  echo "=========="
  echo -e "${message}"
  echo "----------"
  echo "${message}" | cat -e
  echo "----------"
  echo "${message}" | cat -e | sed -e 's/\^\[/\\e/g' -e 's/\$$//'
  echo "=========="
}





function assertStringIsEmpty() {
  assertEquals "" "${1}"
}

function assertStringIsNotEmpty() {
  assertNotEquals "" "${1}"
}

function assertFileExists() {
  local file_path="${1}"
  if [[ ! -f "${file_path}" ]]; then
    fail "Expected the file '${file_path}' to exist, but it does not"
  fi
}

function assertFileDoesNotExist() {
  local file_path="${1}"
  if [[ -f "${file_path}" ]]; then
    fail "Expected the file '${file_path}' to not exist, but it does"
  fi
}

function failIfAnsi() {
  if command -v ansi::color > /dev/null; then
    fail "ANSI library must be absent!"
  fi
}
