#!/usr/bin/env bash



# ------------------------------------------------------------------------------
shared::test_message_default_date_format() {
  local date_before
  local actual_message
  local date_after

  # Should take less than one second
  date_before=$(date "+%F %T")
  actual_message=$(log error "This is a log message")
  date_after=$(date "+%F %T")

  actual_message=$(_strip_escape_codes "${actual_message}")

  local expected_message_1="${date_before} [ERROR] This is a log message"
  local expected_message_2="${date_after} [ERROR] This is a log message"

  if [[ "${actual_message}" != "${expected_message_1}" ]] && [[ "${actual_message}" != "${expected_message_2}" ]]; then
    fail "The log message does not contain the expected formatted date"
  fi
}

# ------------------------------------------------------------------------------
shared::test_message_custom_date_format() {
  ndd::logger::set_stdout_date_format "+%Y%m%d%H%M%S"

  local date_before
  local actual_message
  local date_after

  # Should take less than one second
  date_before=$(date "+%Y%m%d%H%M%S")
  actual_message=$(log error "This is a log message")
  date_after=$(date "+%Y%m%d%H%M%S")

  actual_message=$(_strip_escape_codes "${actual_message}")

  local expected_message_1="${date_before} [ERROR] This is a log message"
  local expected_message_2="${date_after} [ERROR] This is a log message"

  if [[ "${actual_message}" != "${expected_message_1}" ]] && [[ "${actual_message}" != "${expected_message_2}" ]]; then
    fail "The log message does not contain the expected formatted date"
  fi
}



# ------------------------------------------------------------------------------
shared::test_message_default_log_format() {
  local actual_message

  actual_message=$(log error "This is a log message")
  actual_message=$(_strip_escape_codes "${actual_message}")
  actual_message=$(_replace_default_date "${actual_message}")

  assertEquals "_DATE_ [ERROR] This is a log message" "${actual_message}"
}

# ------------------------------------------------------------------------------
shared::test_message_custom_log_format() {
  ndd::logger::set_stdout_log_format "%s | %-5s | %s"

  local actual_message

  actual_message=$(log error "This is a log message")
  actual_message=$(_strip_escape_codes "${actual_message}")
  actual_message=$(_replace_default_date "${actual_message}")

  assertEquals "_DATE_ | ERROR | This is a log message" "${actual_message}"
}



# ------------------------------------------------------------------------------
shared::test_message_level_at_logger_level_disabled() {
  ndd::logger::set_stdout_level "DISABLED"

  assertStringIsEmpty "$(log emergency   "This is a EMERGENCY log message")"
  assertStringIsEmpty "$(log alert       "This is a ALERT log message")"
  assertStringIsEmpty "$(log critical    "This is a CRITICAL log message")"
  assertStringIsEmpty "$(log error       "This is a ERROR log message")"
  assertStringIsEmpty "$(log warning     "This is a WARNING log message")"
  assertStringIsEmpty "$(log notice      "This is a NOTICE log message")"
  assertStringIsEmpty "$(log information "This is a INFORMATION log message")"
  assertStringIsEmpty "$(log debug       "This is a DEBUG log message")"
}

# ------------------------------------------------------------------------------
shared::test_message_level_at_logger_level_emergency() {
  ndd::logger::set_stdout_level "EMERGENCY"

  assertEquals "_DATE_ [EMERG] This is a EMERGENCY log message"    "$( _normalize "$(log emergency   "This is a EMERGENCY log message")" )"

  assertStringIsEmpty "$(log alert       "This is a ALERT log message")"
  assertStringIsEmpty "$(log critical    "This is a CRITICAL log message")"
  assertStringIsEmpty "$(log error       "This is a ERROR log message")"
  assertStringIsEmpty "$(log warning     "This is a WARNING log message")"
  assertStringIsEmpty "$(log notice      "This is a NOTICE log message")"
  assertStringIsEmpty "$(log information "This is a INFORMATION log message")"
  assertStringIsEmpty "$(log debug       "This is a DEBUG log message")"
}

# ------------------------------------------------------------------------------
shared::test_message_level_at_logger_level_alert() {
  ndd::logger::set_stdout_level "ALERT"

  assertEquals "_DATE_ [EMERG] This is a EMERGENCY log message"    "$( _normalize "$(log emergency   "This is a EMERGENCY log message")" )"
  assertEquals "_DATE_ [ALERT] This is a ALERT log message"        "$( _normalize "$(log alert       "This is a ALERT log message")" )"

  assertStringIsEmpty "$(log critical    "This is a CRITICAL log message")"
  assertStringIsEmpty "$(log error       "This is a ERROR log message")"
  assertStringIsEmpty "$(log warning     "This is a WARNING log message")"
  assertStringIsEmpty "$(log notice      "This is a NOTICE log message")"
  assertStringIsEmpty "$(log information "This is a INFORMATION log message")"
  assertStringIsEmpty "$(log debug       "This is a DEBUG log message")"
}

# ------------------------------------------------------------------------------
shared::test_message_level_at_logger_level_critical() {
  ndd::logger::set_stdout_level "CRITICAL"

  assertEquals "_DATE_ [EMERG] This is a EMERGENCY log message"    "$( _normalize "$(log emergency   "This is a EMERGENCY log message")" )"
  assertEquals "_DATE_ [ALERT] This is a ALERT log message"        "$( _normalize "$(log alert       "This is a ALERT log message")" )"
  assertEquals "_DATE_ [CRIT ] This is a CRITICAL log message"     "$( _normalize "$(log critical    "This is a CRITICAL log message")" )"

  assertStringIsEmpty "$(log error       "This is a ERROR log message")"
  assertStringIsEmpty "$(log warning     "This is a WARNING log message")"
  assertStringIsEmpty "$(log notice      "This is a NOTICE log message")"
  assertStringIsEmpty "$(log information "This is a INFORMATION log message")"
  assertStringIsEmpty "$(log debug       "This is a DEBUG log message")"
}

# ------------------------------------------------------------------------------
shared::test_message_level_at_logger_level_error() {
  ndd::logger::set_stdout_level "ERROR"

  assertEquals "_DATE_ [EMERG] This is a EMERGENCY log message"    "$( _normalize "$(log emergency   "This is a EMERGENCY log message")" )"
  assertEquals "_DATE_ [ALERT] This is a ALERT log message"        "$( _normalize "$(log alert       "This is a ALERT log message")" )"
  assertEquals "_DATE_ [CRIT ] This is a CRITICAL log message"     "$( _normalize "$(log critical    "This is a CRITICAL log message")" )"
  assertEquals "_DATE_ [ERROR] This is a ERROR log message"        "$( _normalize "$(log error       "This is a ERROR log message")" )"

  assertStringIsEmpty "$(log warning     "This is a WARNING log message")"
  assertStringIsEmpty "$(log notice      "This is a NOTICE log message")"
  assertStringIsEmpty "$(log information "This is a INFORMATION log message")"
  assertStringIsEmpty "$(log debug       "This is a DEBUG log message")"
}

# ------------------------------------------------------------------------------
shared::test_message_level_at_logger_level_warning() {
  ndd::logger::set_stdout_level "WARNING"

  assertEquals "_DATE_ [EMERG] This is a EMERGENCY log message"    "$( _normalize "$(log emergency   "This is a EMERGENCY log message")" )"
  assertEquals "_DATE_ [ALERT] This is a ALERT log message"        "$( _normalize "$(log alert       "This is a ALERT log message")" )"
  assertEquals "_DATE_ [CRIT ] This is a CRITICAL log message"     "$( _normalize "$(log critical    "This is a CRITICAL log message")" )"
  assertEquals "_DATE_ [ERROR] This is a ERROR log message"        "$( _normalize "$(log error       "This is a ERROR log message")" )"
  assertEquals "_DATE_ [WARN ] This is a WARNING log message"      "$( _normalize "$(log warning     "This is a WARNING log message")" )"

  assertStringIsEmpty "$(log notice      "This is a NOTICE log message")"
  assertStringIsEmpty "$(log information "This is a INFORMATION log message")"
  assertStringIsEmpty "$(log debug       "This is a DEBUG log message")"
}

# ------------------------------------------------------------------------------
shared::test_message_level_at_logger_level_notice() {
  ndd::logger::set_stdout_level "NOTICE"

  assertEquals "_DATE_ [EMERG] This is a EMERGENCY log message"    "$( _normalize "$(log emergency   "This is a EMERGENCY log message")" )"
  assertEquals "_DATE_ [ALERT] This is a ALERT log message"        "$( _normalize "$(log alert       "This is a ALERT log message")" )"
  assertEquals "_DATE_ [CRIT ] This is a CRITICAL log message"     "$( _normalize "$(log critical    "This is a CRITICAL log message")" )"
  assertEquals "_DATE_ [ERROR] This is a ERROR log message"        "$( _normalize "$(log error       "This is a ERROR log message")" )"
  assertEquals "_DATE_ [WARN ] This is a WARNING log message"      "$( _normalize "$(log warning     "This is a WARNING log message")" )"
  assertEquals "_DATE_ [NOTIC] This is a NOTICE log message"       "$( _normalize "$(log notice      "This is a NOTICE log message")" )"

  assertStringIsEmpty "$(log information "This is a INFORMATION log message")"
  assertStringIsEmpty "$(log debug       "This is a DEBUG log message")"
}

# ------------------------------------------------------------------------------
shared::test_message_level_at_logger_level_information() {
  ndd::logger::set_stdout_level "INFORMATION"

  assertEquals "_DATE_ [EMERG] This is a EMERGENCY log message"    "$( _normalize "$(log emergency   "This is a EMERGENCY log message")" )"
  assertEquals "_DATE_ [ALERT] This is a ALERT log message"        "$( _normalize "$(log alert       "This is a ALERT log message")" )"
  assertEquals "_DATE_ [CRIT ] This is a CRITICAL log message"     "$( _normalize "$(log critical    "This is a CRITICAL log message")" )"
  assertEquals "_DATE_ [ERROR] This is a ERROR log message"        "$( _normalize "$(log error       "This is a ERROR log message")" )"
  assertEquals "_DATE_ [WARN ] This is a WARNING log message"      "$( _normalize "$(log warning     "This is a WARNING log message")" )"
  assertEquals "_DATE_ [NOTIC] This is a NOTICE log message"       "$( _normalize "$(log notice      "This is a NOTICE log message")" )"
  assertEquals "_DATE_ [INFO ] This is a INFORMATION log message"  "$( _normalize "$(log information "This is a INFORMATION log message")" )"

  assertStringIsEmpty "$(log debug       "This is a DEBUG log message")"
}

# ------------------------------------------------------------------------------
shared::test_message_level_at_logger_level_debug() {
  ndd::logger::set_stdout_level "DEBUG"

  assertEquals "_DATE_ [EMERG] This is a EMERGENCY log message"    "$( _normalize "$(log emergency   "This is a EMERGENCY log message")" )"
  assertEquals "_DATE_ [ALERT] This is a ALERT log message"        "$( _normalize "$(log alert       "This is a ALERT log message")" )"
  assertEquals "_DATE_ [CRIT ] This is a CRITICAL log message"     "$( _normalize "$(log critical    "This is a CRITICAL log message")" )"
  assertEquals "_DATE_ [ERROR] This is a ERROR log message"        "$( _normalize "$(log error       "This is a ERROR log message")" )"
  assertEquals "_DATE_ [WARN ] This is a WARNING log message"      "$( _normalize "$(log warning     "This is a WARNING log message")" )"
  assertEquals "_DATE_ [NOTIC] This is a NOTICE log message"       "$( _normalize "$(log notice      "This is a NOTICE log message")" )"
  assertEquals "_DATE_ [INFO ] This is a INFORMATION log message"  "$( _normalize "$(log information "This is a INFORMATION log message")" )"
  assertEquals "_DATE_ [DEBUG] This is a DEBUG log message"        "$( _normalize "$(log debug       "This is a DEBUG log message")" )"
}

# ------------------------------------------------------------------------------
shared::test_message_level_synonyms() {
  ndd::logger::set_stdout_level "DEBUG"

  assertEquals "_DATE_ [EMERG] This is a EMERGENCY log message"    "$( _normalize "$(log emerg "This is a EMERGENCY log message")" )"
  assertEquals "_DATE_ [CRIT ] This is a CRITICAL log message"     "$( _normalize "$(log crit  "This is a CRITICAL log message")" )"
  assertEquals "_DATE_ [WARN ] This is a WARNING log message"      "$( _normalize "$(log warn  "This is a WARNING log message")" )"
  assertEquals "_DATE_ [INFO ] This is a INFORMATION log message"  "$( _normalize "$(log info  "This is a INFORMATION log message")" )"
}

# ------------------------------------------------------------------------------
shared::test_message_with_interpolation_without_special_character() {
  ndd::logger::set_stdout_level "DEBUG"

  assertEquals "_DATE_ [EMERG] This is a EMERGENCY   log message"  "$( _normalize "$(log emergency   "This is a %-11s log message" "EMERGENCY"  )" )"
  assertEquals "_DATE_ [ALERT] This is a ALERT       log message"  "$( _normalize "$(log alert       "This is a %-11s log message" "ALERT"      )" )"
  assertEquals "_DATE_ [CRIT ] This is a CRITICAL    log message"  "$( _normalize "$(log critical    "This is a %-11s log message" "CRITICAL"   )" )"
  assertEquals "_DATE_ [ERROR] This is a ERROR       log message"  "$( _normalize "$(log error       "This is a %-11s log message" "ERROR"      )" )"
  assertEquals "_DATE_ [WARN ] This is a WARNING     log message"  "$( _normalize "$(log warning     "This is a %-11s log message" "WARNING"    )" )"
  assertEquals "_DATE_ [NOTIC] This is a NOTICE      log message"  "$( _normalize "$(log notice      "This is a %-11s log message" "NOTICE"     )" )"
  assertEquals "_DATE_ [INFO ] This is a INFORMATION log message"  "$( _normalize "$(log information "This is a %-11s log message" "INFORMATION")" )"
  assertEquals "_DATE_ [DEBUG] This is a DEBUG       log message"  "$( _normalize "$(log debug       "This is a %-11s log message" "DEBUG"      )" )"
}

# ------------------------------------------------------------------------------
shared::test_message_with_interpolation_with_special_character() {
  ndd::logger::set_stdout_level "DEBUG"

  assertEquals "_DATE_ [EMERG] - This is a EMERGENCY   log message"  "$( _normalize "$(log emergency   "%s This is a %-11s log message" "-" "EMERGENCY"  )" )"
  assertEquals "_DATE_ [ALERT] - This is a ALERT       log message"  "$( _normalize "$(log alert       "%s This is a %-11s log message" "-" "ALERT"      )" )"
  assertEquals "_DATE_ [CRIT ] - This is a CRITICAL    log message"  "$( _normalize "$(log critical    "%s This is a %-11s log message" "-" "CRITICAL"   )" )"
  assertEquals "_DATE_ [ERROR] - This is a ERROR       log message"  "$( _normalize "$(log error       "%s This is a %-11s log message" "-" "ERROR"      )" )"
  assertEquals "_DATE_ [WARN ] - This is a WARNING     log message"  "$( _normalize "$(log warning     "%s This is a %-11s log message" "-" "WARNING"    )" )"
  assertEquals "_DATE_ [NOTIC] - This is a NOTICE      log message"  "$( _normalize "$(log notice      "%s This is a %-11s log message" "-" "NOTICE"     )" )"
  assertEquals "_DATE_ [INFO ] - This is a INFORMATION log message"  "$( _normalize "$(log information "%s This is a %-11s log message" "-" "INFORMATION")" )"
  assertEquals "_DATE_ [DEBUG] - This is a DEBUG       log message"  "$( _normalize "$(log debug       "%s This is a %-11s log message" "-" "DEBUG"      )" )"
}

# ------------------------------------------------------------------------------
shared::test_message_without_interpolation_with_special_character() {
  ndd::logger::set_stdout_level "DEBUG"

  assertEquals "_DATE_ [EMERG] - This is a EMERGENCY   log message"  "$( _normalize "$(log emergency   "- This is a EMERGENCY   log message" )" )"
  assertEquals "_DATE_ [ALERT] - This is a ALERT       log message"  "$( _normalize "$(log alert       "- This is a ALERT       log message" )" )"
  assertEquals "_DATE_ [CRIT ] - This is a CRITICAL    log message"  "$( _normalize "$(log critical    "- This is a CRITICAL    log message" )" )"
  assertEquals "_DATE_ [ERROR] - This is a ERROR       log message"  "$( _normalize "$(log error       "- This is a ERROR       log message" )" )"
  assertEquals "_DATE_ [WARN ] - This is a WARNING     log message"  "$( _normalize "$(log warning     "- This is a WARNING     log message" )" )"
  assertEquals "_DATE_ [NOTIC] - This is a NOTICE      log message"  "$( _normalize "$(log notice      "- This is a NOTICE      log message" )" )"
  assertEquals "_DATE_ [INFO ] - This is a INFORMATION log message"  "$( _normalize "$(log information "- This is a INFORMATION log message" )" )"
  assertEquals "_DATE_ [DEBUG] - This is a DEBUG       log message"  "$( _normalize "$(log debug       "- This is a DEBUG       log message" )" )"
}

# ------------------------------------------------------------------------------
shared::test_invalid_maximum_logger_level() {
  local return_value
  (ndd::logger::set_stdout_level "INVALID" >/dev/null) || return_value="${?}"
  assertEquals 1 ${return_value}
}

# ------------------------------------------------------------------------------
shared::test_invalid_logger_level() {
  ndd::logger::set_stdout_level "DEBUG"

  local return_value
  (log invalid "This is a log message" >/dev/null) || return_value="${?}"
  assertEquals 1 ${return_value}
}

# ------------------------------------------------------------------------------
shared::test_profile_json_for_stdout() {
  ndd::logger::set_stdout_level "DEBUG"

  ndd::logger::profile::use_json_for_stdout

  actual_message=$(log error "This is a log message")
  actual_message=$(_strip_escape_codes "${actual_message}")
  actual_message=$(_replace_timestamp "${actual_message}")
  assertEquals '{"timestamp":"_TIMESTAMP_","level":"ERROR","message":"This is a log message"}' "${actual_message}"
}
