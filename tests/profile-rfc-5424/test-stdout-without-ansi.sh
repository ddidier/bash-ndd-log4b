#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
# source "${PROJECT_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/ndd-log4b.sh"



echo "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ tests/profile-rfc5424/test-without-ansi.sh ━━━━━"



# shellcheck disable=SC1090
source "${PROJECT_DIR}/tests/_shared.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/tests/profile-rfc-5424/_shared-stdout.sh"



setUp() {
  failIfAnsi

  ndd::logger::profiles::use_rfc_5424

  ndd::logger::set_stdout_level "${NDD_LOG4B_STDOUT_DEFAULT_LEVEL}"
  ndd::logger::set_stdout_date_format "${NDD_LOG4B_STDOUT_DEFAULT_DATE_FORMAT}"
  ndd::logger::set_stdout_log_format "${NDD_LOG4B_STDOUT_DEFAULT_LOG_FORMAT}"
}



# ------------------------------------------------------------------------------
test_message_default_date_format() {
  shared::test_message_default_date_format
}

# ------------------------------------------------------------------------------
test_message_custom_date_format() {
  shared::test_message_custom_date_format
}



# ------------------------------------------------------------------------------
test_message_default_log_format() {
  shared::test_message_default_log_format
}

# ------------------------------------------------------------------------------
test_message_custom_log_format() {
  shared::test_message_custom_log_format
}



# ------------------------------------------------------------------------------
test_message_level_at_logger_level_disabled() {
  shared::test_message_level_at_logger_level_disabled
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_emergency() {
  shared::test_message_level_at_logger_level_emergency
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_alert() {
  shared::test_message_level_at_logger_level_alert
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_critical() {
  shared::test_message_level_at_logger_level_critical
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_error() {
  shared::test_message_level_at_logger_level_error
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_warning() {
  shared::test_message_level_at_logger_level_warning
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_notice() {
  shared::test_message_level_at_logger_level_notice
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_information() {
  shared::test_message_level_at_logger_level_information
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_debug() {
  shared::test_message_level_at_logger_level_debug
}



# ------------------------------------------------------------------------------
test_message_level_synonyms() {
  shared::test_message_level_synonyms
}

# ------------------------------------------------------------------------------
test_message_with_interpolation_without_special_character() {
  shared::test_message_with_interpolation_without_special_character
}

# ------------------------------------------------------------------------------
test_message_with_interpolation_with_special_character() {
  shared::test_message_with_interpolation_with_special_character
}

# ------------------------------------------------------------------------------
test_message_without_interpolation_with_special_character() {
  shared::test_message_without_interpolation_with_special_character
}



# ------------------------------------------------------------------------------
test_invalid_maximum_logger_level() {
  shared::test_invalid_maximum_logger_level
}

# ------------------------------------------------------------------------------
test_invalid_logger_level() {
  shared::test_invalid_logger_level
}



# ------------------------------------------------------------------------------
test_profile_json_for_stdout() {
  shared::test_profile_json_for_stdout
}



# ------------------------------------------------------------------------------
test_message_color() {
  ndd::logger::set_stdout_level "DEBUG"

  local actual_message
  local expected_message

  actual_message=$(log emergency "This is a EMERGENCY log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message="_DATE_ [EMERG] This is a EMERGENCY log message"
  assertEquals "${expected_message}" "${actual_message}"

  actual_message=$(log alert "This is a ALERT log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message="_DATE_ [ALERT] This is a ALERT log message"
  assertEquals "${expected_message}" "${actual_message}"

  actual_message=$(log critical "This is a CRITICAL log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message="_DATE_ [CRIT ] This is a CRITICAL log message"
  assertEquals "${expected_message}" "${actual_message}"

  actual_message=$(log error "This is a ERROR log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message="_DATE_ [ERROR] This is a ERROR log message"
  assertEquals "${expected_message}" "${actual_message}"

  actual_message=$(log warning "This is a WARNING log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message="_DATE_ [WARN ] This is a WARNING log message"
  assertEquals "${expected_message}" "${actual_message}"

  actual_message=$(log notice "This is a NOTICE log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message="_DATE_ [NOTIC] This is a NOTICE log message"
  assertEquals "${expected_message}" "${actual_message}"

  actual_message=$(log information "This is a INFORMATION log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message="_DATE_ [INFO ] This is a INFORMATION log message"
  assertEquals "${expected_message}" "${actual_message}"

  actual_message=$(log debug "This is a DEBUG log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message="_DATE_ [DEBUG] This is a DEBUG log message"
  assertEquals "${expected_message}" "${actual_message}"
}



# ------------------------------------------------------------------------------
test_print_levels() {
  local actual_message
  actual_message="$(ndd::logger::print_levels)"
  # _debug_escape_codes "$(ndd::logger::print_levels)"

  assertContains "${actual_message}"    "Source the 'ansi' library (https://github.com/fidian/ansi/) to add colors!"

  assertContains "${actual_message}"    "0 EMERGENCY    Some text as example "
  assertContains "${actual_message}"    "1 ALERT        Some text as example "
  assertContains "${actual_message}"    "2 CRITICAL     Some text as example "
  assertContains "${actual_message}"    "3 ERROR        Some text as example "
  assertContains "${actual_message}"    "4 WARNING      Some text as example "
  assertContains "${actual_message}"    "5 NOTICE       Some text as example "
  assertContains "${actual_message}"    "6 INFORMATION  Some text as example "
  assertContains "${actual_message}"    "7 DEBUG        Some text as example "
}



# ------------------------------------------------------------------------------

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/shunit2/shunit2"
