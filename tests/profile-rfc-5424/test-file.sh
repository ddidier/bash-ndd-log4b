#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/ndd-log4b.sh"



echo "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ tests/profile-rfc5424/test-file.sh ━━━━━"



# shellcheck disable=SC1090
source "${PROJECT_DIR}/tests/_shared.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/tests/profile-rfc-5424/_shared-stdout.sh"



setUp() {
  LOG_FILE_PATH="${SHUNIT_TMPDIR}/text-file.log"

  ndd::logger::profiles::use_rfc_5424

  ndd::logger::set_stdout_level "DISABLED"
  ndd::logger::set_file_level "WARNING"
  ndd::logger::set_file_date_format "${NDD_LOG4B_FILE_DEFAULT_DATE_FORMAT}"
  ndd::logger::set_file_log_format "${NDD_LOG4B_FILE_DEFAULT_LOG_FORMAT}"
  ndd::logger::set_file_path "${LOG_FILE_PATH}"

  rm -f "${LOG_FILE_PATH}"
}



# ------------------------------------------------------------------------------
test_message_default_date_format() {
  local date_before
  local date_after

  # Should take less than one second
  date_before=$(date "+%F %T")
  log error "This is a log message"
  date_after=$(date "+%F %T")

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")

  local expected_message_1="${date_before} [ERROR] This is a log message$"
  local expected_message_2="${date_after} [ERROR] This is a log message$"

  if [[ "${actual_message}" != "${expected_message_1}" ]] && [[ "${actual_message}" != "${expected_message_2}" ]]; then
    fail "The log message does not contain the expected formatted date"
  fi
}

# ------------------------------------------------------------------------------
test_message_custom_date_format() {
  ndd::logger::set_file_date_format "+%Y%m%d%H%M%S"

  local date_before
  local date_after

  # Should take less than one second
  date_before=$(date "+%Y%m%d%H%M%S")
  log error "This is a log message"
  date_after=$(date "+%Y%m%d%H%M%S")

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")

  local expected_message_1="${date_before} [ERROR] This is a log message$"
  local expected_message_2="${date_after} [ERROR] This is a log message$"

  if [[ "${actual_message}" != "${expected_message_1}" ]] && [[ "${actual_message}" != "${expected_message_2}" ]]; then
    fail "The log message does not contain the expected formatted date"
  fi
}



# ------------------------------------------------------------------------------
test_message_default_log_format() {
  log error "This is a log message"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_strip_escape_codes "${actual_message}")
  actual_message=$(_replace_default_date "${actual_message}")

  assertEquals "_DATE_ [ERROR] This is a log message$" "${actual_message}"
}

# ------------------------------------------------------------------------------
test_message_custom_log_format() {
  ndd::logger::set_file_log_format "%s | %-5s | %s"

  log error "This is a log message"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_strip_escape_codes "${actual_message}")
  actual_message=$(_replace_default_date "${actual_message}")

  assertEquals "_DATE_ | ERROR | This is a log message$" "${actual_message}"
}



# ------------------------------------------------------------------------------
test_message_level_at_logger_level_disabled() {
  ndd::logger::set_file_level "DISABLED"

  log emergency   "This is a EMERGENCY log message"
  log alert       "This is a ALERT log message"
  log critical    "This is a CRITICAL log message"
  log error       "This is a ERROR log message"
  log warning     "This is a WARNING log message"
  log notice      "This is a NOTICE log message"
  log information "This is a INFORMATION log message"
  log debug       "This is a DEBUG log message"

  assertFileDoesNotExist "${LOG_FILE_PATH}"
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_emergency() {
  ndd::logger::set_file_level "EMERGENCY"

  log emergency   "This is a EMERGENCY log message"
  log alert       "This is a ALERT log message"
  log critical    "This is a CRITICAL log message"
  log error       "This is a ERROR log message"
  log warning     "This is a WARNING log message"
  log notice      "This is a NOTICE log message"
  log information "This is a INFORMATION log message"
  log debug       "This is a DEBUG log message"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_default_date "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_level_at_logger_level_emergency.txt")

  assertEquals  "${expected_message}"  "${actual_message}"
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_alert() {
  ndd::logger::set_file_level "ALERT"

  log emergency   "This is a EMERGENCY log message"
  log alert       "This is a ALERT log message"
  log critical    "This is a CRITICAL log message"
  log error       "This is a ERROR log message"
  log warning     "This is a WARNING log message"
  log notice      "This is a NOTICE log message"
  log information "This is a INFORMATION log message"
  log debug       "This is a DEBUG log message"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_default_date "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_level_at_logger_level_alert.txt")

  assertEquals  "${expected_message}"  "${actual_message}"
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_critical() {
  ndd::logger::set_file_level "CRITICAL"

  log emergency   "This is a EMERGENCY log message"
  log alert       "This is a ALERT log message"
  log critical    "This is a CRITICAL log message"
  log error       "This is a ERROR log message"
  log warning     "This is a WARNING log message"
  log notice      "This is a NOTICE log message"
  log information "This is a INFORMATION log message"
  log debug       "This is a DEBUG log message"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_default_date "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_level_at_logger_level_critical.txt")

  assertEquals  "${expected_message}"  "${actual_message}"
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_error() {
  ndd::logger::set_file_level "ERROR"

  log emergency   "This is a EMERGENCY log message"
  log alert       "This is a ALERT log message"
  log critical    "This is a CRITICAL log message"
  log error       "This is a ERROR log message"
  log warning     "This is a WARNING log message"
  log notice      "This is a NOTICE log message"
  log information "This is a INFORMATION log message"
  log debug       "This is a DEBUG log message"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_default_date "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_level_at_logger_level_error.txt")

  assertEquals  "${expected_message}"  "${actual_message}"
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_warning() {
  ndd::logger::set_file_level "WARNING"

  log emergency   "This is a EMERGENCY log message"
  log alert       "This is a ALERT log message"
  log critical    "This is a CRITICAL log message"
  log error       "This is a ERROR log message"
  log warning     "This is a WARNING log message"
  log notice      "This is a NOTICE log message"
  log information "This is a INFORMATION log message"
  log debug       "This is a DEBUG log message"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_default_date "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_level_at_logger_level_warning.txt")

  assertEquals  "${expected_message}"  "${actual_message}"
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_notice() {
  ndd::logger::set_file_level "NOTICE"

  log emergency   "This is a EMERGENCY log message"
  log alert       "This is a ALERT log message"
  log critical    "This is a CRITICAL log message"
  log error       "This is a ERROR log message"
  log warning     "This is a WARNING log message"
  log notice      "This is a NOTICE log message"
  log information "This is a INFORMATION log message"
  log debug       "This is a DEBUG log message"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_default_date "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_level_at_logger_level_notice.txt")

  assertEquals  "${expected_message}"  "${actual_message}"
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_information() {
  ndd::logger::set_file_level "INFORMATION"

  log emergency   "This is a EMERGENCY log message"
  log alert       "This is a ALERT log message"
  log critical    "This is a CRITICAL log message"
  log error       "This is a ERROR log message"
  log warning     "This is a WARNING log message"
  log notice      "This is a NOTICE log message"
  log information "This is a INFORMATION log message"
  log debug       "This is a DEBUG log message"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_default_date "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_level_at_logger_level_information.txt")

  assertEquals  "${expected_message}"  "${actual_message}"
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_debug() {
  ndd::logger::set_file_level "DEBUG"

  log emergency   "This is a EMERGENCY log message"
  log alert       "This is a ALERT log message"
  log critical    "This is a CRITICAL log message"
  log error       "This is a ERROR log message"
  log warning     "This is a WARNING log message"
  log notice      "This is a NOTICE log message"
  log information "This is a INFORMATION log message"
  log debug       "This is a DEBUG log message"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_default_date "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_level_at_logger_level_debug.txt")

  assertEquals  "${expected_message}"  "${actual_message}"
}

# ------------------------------------------------------------------------------
test_message_level_synonyms() {
  ndd::logger::set_file_level "DEBUG"

  log emerg "This is a EMERGENCY log message"
  log crit  "This is a CRITICAL log message"
  log warn  "This is a WARNING log message"
  log info  "This is a INFORMATION log message"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_default_date "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_level_synonyms.txt")

  assertEquals  "${expected_message}"  "${actual_message}"
}

# ------------------------------------------------------------------------------
test_message_with_interpolation_without_special_character() {
  ndd::logger::set_file_level "DEBUG"

  log emergency   "This is a %-11s log message" "EMERGENCY"
  log alert       "This is a %-11s log message" "ALERT"
  log critical    "This is a %-11s log message" "CRITICAL"
  log error       "This is a %-11s log message" "ERROR"
  log warning     "This is a %-11s log message" "WARNING"
  log notice      "This is a %-11s log message" "NOTICE"
  log information "This is a %-11s log message" "INFORMATION"
  log debug       "This is a %-11s log message" "DEBUG"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_default_date "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_interpolation.txt")

  assertEquals  "${expected_message}"  "${actual_message}"
}

# ------------------------------------------------------------------------------
test_message_with_interpolation_with_special_character() {
  ndd::logger::set_file_level "DEBUG"

  log emergency   "%s This is a %-11s log message" "-" "EMERGENCY"
  log alert       "%s This is a %-11s log message" "-" "ALERT"
  log critical    "%s This is a %-11s log message" "-" "CRITICAL"
  log error       "%s This is a %-11s log message" "-" "ERROR"
  log warning     "%s This is a %-11s log message" "-" "WARNING"
  log notice      "%s This is a %-11s log message" "-" "NOTICE"
  log information "%s This is a %-11s log message" "-" "INFORMATION"
  log debug       "%s This is a %-11s log message" "-" "DEBUG"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_default_date "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_interpolation_dash.txt")

  assertEquals  "${expected_message}"  "${actual_message}"
}

# ------------------------------------------------------------------------------
test_message_without_interpolation_with_special_character() {
  ndd::logger::set_file_level "DEBUG"

  log emergency   "- This is a EMERGENCY   log message"
  log alert       "- This is a ALERT       log message"
  log critical    "- This is a CRITICAL    log message"
  log error       "- This is a ERROR       log message"
  log warning     "- This is a WARNING     log message"
  log notice      "- This is a NOTICE      log message"
  log information "- This is a INFORMATION log message"
  log debug       "- This is a DEBUG       log message"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_default_date "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_interpolation_dash.txt")

  assertEquals  "${expected_message}"  "${actual_message}"
}

# ------------------------------------------------------------------------------
test_invalid_maximum_logger_level() {
  local return_value
  (ndd::logger::set_file_level "INVALID" >/dev/null) || return_value="${?}"
  assertEquals 1 ${return_value}
}

# ------------------------------------------------------------------------------
test_invalid_logger_level() {
  ndd::logger::set_file_level "DEBUG"

  local return_value
  (log invalid "This is a log message" >/dev/null) || return_value="${?}"
  assertEquals 1 ${return_value}
}

# ------------------------------------------------------------------------------
test_missing_file() {
  ndd::logger::set_file_level "DEBUG"

  ndd::logger::set_file_path ""

  local return_value
  (log error "This is a log message" >/dev/null) || return_value="${?}"
  assertEquals 1 ${return_value}
}



# ------------------------------------------------------------------------------
test_profile_json_for_file() {
  ndd::logger::set_file_level "DEBUG"

  ndd::logger::profile::use_json_for_file

  log emergency   "This is a %-11s log message" "EMERGENCY"
  log alert       "This is a %-11s log message" "ALERT"
  log critical    "This is a %-11s log message" "CRITICAL"
  log error       "This is a %-11s log message" "ERROR"
  log warning     "This is a %-11s log message" "WARNING"
  log notice      "This is a %-11s log message" "NOTICE"
  log information "This is a %-11s log message" "INFORMATION"
  log debug       "This is a %-11s log message" "DEBUG"

  local actual_message
  actual_message=$(cat -e "${LOG_FILE_PATH}")
  actual_message=$(_replace_timestamp "${actual_message}")

  local expected_message
  expected_message=$(cat -e "${PROJECT_DIR}/tests/profile-rfc-5424/expected-files/test_message_custom_log_format_as_json.json")

  assertEquals  "${expected_message}"  "${actual_message}"
}



# ------------------------------------------------------------------------------

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/shunit2/shunit2"
