#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/ndd-log4b.sh"



echo "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ tests/profile-log4j/test-sourced.sh ━━━━━"



# ------------------------------------------------------------------------------
test_cannot_be_sourced_twice() {

  assertEquals "255,0,0" "${_NDD_LOG4B_COLOR_RED}"

  _NDD_LOG4B_COLOR_RED="0,0,0"

  assertEquals "0,0,0" "${_NDD_LOG4B_COLOR_RED}"

  # shellcheck disable=SC1090
  source "${PROJECT_DIR}/src/ndd-log4b.sh"

  assertEquals "0,0,0" "${_NDD_LOG4B_COLOR_RED}"
}



# ------------------------------------------------------------------------------

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/shunit2/shunit2"
