#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=1090
source "${PROJECT_DIR}/lib/ansi/ansi"


function main() {

  local stage_name="${1:-Test}"

  echo "Checking uncommitted sources"

  if git diff --quiet; then
    echo "Sources have not been modified: nothing to commit"
  else
    echo "Sources have been modified: committing changes"
    git add --all
    git commit -m "Automatic commit for GitLab Runner testing"
  fi

  echo "Creating temporary directory"

  local temp_dir
  local temp_dir_link
  temp_dir=$(mktemp -d --suffix=.gitlab-runner-ndd-log4b)
  temp_dir_link="$(dirname "${temp_dir}")/gitlab-runner-ndd-log4b"

  rm -rf "${temp_dir_link}"
  ln -sf "${temp_dir}" "${temp_dir_link}"

  echo "Test data will be stored in the temporary directory: ${temp_dir}"
  echo "Use the following link for convenience: ${temp_dir_link}"

  echo "Starting GitLab runner for stage '${stage_name}'"

  GITLAB_RUNNER_VERSION=v13.6.0

  docker run \
    --rm -i                                                       \
    -v /opt/gitlab-runner/config:/etc/gitlab-runner               \
    -v /var/run/docker.sock:/var/run/docker.sock                  \
    -v "${PWD}":"${PWD}"                                          \
    --workdir "${PWD}"                                            \
    -e CI_PROJECT_DIR="${PWD}"                                    \
    gitlab/gitlab-runner:"${GITLAB_RUNNER_VERSION}"               \
    exec docker                                                   \
      --docker-privileged                                         \
      --docker-pull-policy="if-not-present"                       \
      --docker-volumes /var/run/docker.sock:/var/run/docker.sock  \
      --docker-volumes "${temp_dir}":/builds                      \
      Test

}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    ansi --bold --red " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    ansi --bold --red " ┃ TESTS -- Failed!                                                             "
    ansi --bold --red " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

    exit 1
}

trap 'error_handler ${?}' ERR

main "${@}"

exit 0
