#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/ndd-log4b.sh"



echo "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ tests/profile-log4j/test-with-ansi.sh ━━━━━"



# shellcheck disable=SC1090
source "${PROJECT_DIR}/tests/_shared.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/tests/profile-log4j/_shared-stdout.sh"



setUp() {
  ndd::logger::profiles::use_log4j

  ndd::logger::set_stdout_level "${NDD_LOG4B_STDOUT_DEFAULT_LEVEL}"
  ndd::logger::set_stdout_date_format "${NDD_LOG4B_STDOUT_DEFAULT_DATE_FORMAT}"
  ndd::logger::set_stdout_log_format "${NDD_LOG4B_STDOUT_DEFAULT_LOG_FORMAT}"
}



# ------------------------------------------------------------------------------
# test_print_all_levels() {
#   NDD_LOG4B_STDOUT_LEVEL="TRACE"

#   log trace       "This is a TRACE log message"
#   log trace       "This is a TRACE log message"
#   log trace       "This is a TRACE log message"
#   log debug       "This is a DEBUG log message"
#   log debug       "This is a DEBUG log message"
#   log trace       "This is a TRACE log message"
#   log debug       "This is a DEBUG log message"
#   log information "This is a INFORMATION log message"
#   log warning     "This is a WARNING log message"
#   log trace       "This is a TRACE log message"
#   log trace       "This is a TRACE log message"
#   log debug       "This is a DEBUG log message"
#   log debug       "This is a DEBUG log message"
#   log error       "This is a ERROR log message"
#   log fatal       "This is a FATAL log message"
# }
# ./tests/profile-log4j/test-stdout-with-ansi.sh -- test_print_all_levels

# ------------------------------------------------------------------------------
# test_performances() {
#   ndd::logger::set_stdout_level "TRACE"

#   local start_ms
#   local end_ms

#   start_ms=$(date +%s%3N)

#   for _ in $(seq 1 10); do
#     log trace       "This is a TRACE log message"
#     log trace       "This is a TRACE log message"
#     log debug       "This is a DEBUG log message"
#     log trace       "This is a TRACE log message"
#     log debug       "This is a DEBUG log message"
#     log information "This is a INFORMATION log message"
#     log warning     "This is a WARNING log message"
#     log trace       "This is a TRACE log message"
#     log error       "This is a ERROR log message"
#     log fatal       "This is a FATAL log message"
#   done

#   end_ms=$(date +%s%3N)

#   echo "Elapsed time: $((end_ms - start_ms)) ms for 100 log statements"

# }
# ./tests/profile-log4j/test-stdout-with-ansi.sh -- test_performances



# ------------------------------------------------------------------------------
test_message_default_date_format() {
  shared::test_message_default_date_format
}

# ------------------------------------------------------------------------------
test_message_custom_date_format() {
  shared::test_message_custom_date_format
}



# ------------------------------------------------------------------------------
test_message_default_log_format() {
  shared::test_message_default_log_format
}

# ------------------------------------------------------------------------------
test_message_custom_log_format() {
  shared::test_message_custom_log_format
}



# ------------------------------------------------------------------------------
test_message_level_at_logger_level_disabled() {
  shared::test_message_level_at_logger_level_disabled
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_fatal() {
  shared::test_message_level_at_logger_level_fatal
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_error() {
  shared::test_message_level_at_logger_level_error
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_warning() {
  shared::test_message_level_at_logger_level_warning
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_information() {
  shared::test_message_level_at_logger_level_information
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_debug() {
  shared::test_message_level_at_logger_level_debug
}

# ------------------------------------------------------------------------------
test_message_level_at_logger_level_trace() {
  shared::test_message_level_at_logger_level_trace
}



# ------------------------------------------------------------------------------
test_message_level_synonyms() {
  shared::test_message_level_synonyms
}

# ------------------------------------------------------------------------------
test_message_with_interpolation_without_special_character() {
  shared::test_message_with_interpolation_without_special_character
}

# ------------------------------------------------------------------------------
test_message_with_interpolation_with_special_character() {
  shared::test_message_with_interpolation_with_special_character
}

# ------------------------------------------------------------------------------
test_message_without_interpolation_with_special_character() {
  shared::test_message_without_interpolation_with_special_character
}



# ------------------------------------------------------------------------------
test_invalid_maximum_logger_level() {
  shared::test_invalid_maximum_logger_level
}

# ------------------------------------------------------------------------------
test_invalid_logger_level() {
  shared::test_invalid_logger_level
}



# ------------------------------------------------------------------------------
test_profile_json_for_stdout() {
  shared::test_profile_json_for_stdout
}



# ------------------------------------------------------------------------------
test_message_color() {
  ndd::logger::set_stdout_level "TRACE"

  local actual_message
  local expected_message

  actual_message=$(log fatal "This is a FATAL log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message=$'\e[30m\e[1m\e[41m_DATE_ [FATAL] This is a FATAL log message\e[22;39;49m'
  assertEquals "${expected_message}" "${actual_message}"

  actual_message=$(log error "This is a ERROR log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message=$'\e[31m\e[1m_DATE_ [ERROR] This is a ERROR log message\e[22;39m'
  assertEquals "${expected_message}" "${actual_message}"

  actual_message=$(log warning "This is a WARNING log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message=$'\e[33m\e[1m_DATE_ [WARN ] This is a WARNING log message\e[22;39m'
  assertEquals "${expected_message}" "${actual_message}"

  actual_message=$(log information "This is a INFORMATION log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message=$'\e[97m\e[1m_DATE_ [INFO ] This is a INFORMATION log message\e[22;39m'
  assertEquals "${expected_message}" "${actual_message}"

  actual_message=$(log debug "This is a DEBUG log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message=$'_DATE_ [DEBUG] This is a DEBUG log message'
  assertEquals "${expected_message}" "${actual_message}"

  actual_message=$(log trace "This is a TRACE log message")
  actual_message=$(_replace_default_date "${actual_message}")
  expected_message=$'_DATE_ [TRACE] This is a TRACE log message'
  assertEquals "${expected_message}" "${actual_message}"

  # The old look and feel with too many colors:

  # actual_message=$(log fatal "This is a FATAL log message")
  # actual_message=$(_replace_default_date "${actual_message}")
  # expected_message=$'\e[30m\e[48;2;255;0;0m\e[1m_DATE_ [FATAL      ] This is a FATAL log message\e[22;39;49m'
  # assertEquals "${expected_message}" "${actual_message}"

  # actual_message=$(log error "This is a ERROR log message")
  # actual_message=$(_replace_default_date "${actual_message}")
  # expected_message=$'\e[38;2;255;0;0m_DATE_ [ERROR      ] This is a ERROR log message\e[39m'
  # assertEquals "${expected_message}" "${actual_message}"

  # actual_message=$(log warning "This is a WARNING log message")
  # actual_message=$(_replace_default_date "${actual_message}")
  # expected_message=$'\e[38;2;255;127;0m_DATE_ [WARNING    ] This is a WARNING log message\e[39m'
  # assertEquals "${expected_message}" "${actual_message}"

  # actual_message=$(log information "This is a INFORMATION log message")
  # actual_message=$(_replace_default_date "${actual_message}")
  # expected_message=$'\e[38;2;255;255;0m_DATE_ [INFORMATION] This is a INFORMATION log message\e[39m'
  # assertEquals "${expected_message}" "${actual_message}"

  # actual_message=$(log debug "This is a DEBUG log message")
  # actual_message=$(_replace_default_date "${actual_message}")
  # expected_message=$'\e[38;2;0;255;0m_DATE_ [DEBUG      ] This is a DEBUG log message\e[39m'
  # assertEquals "${expected_message}" "${actual_message}"

  # actual_message=$(log trace "This is a TRACE log message")
  # actual_message=$(_replace_default_date "${actual_message}")
  # expected_message=$'\e[38;2;0;128;255m_DATE_ [TRACE      ] This is a TRACE log message\e[39m'
  # assertEquals "${expected_message}" "${actual_message}"
}



# ------------------------------------------------------------------------------
test_print_levels() {
  local actual_message
  actual_message="$(ndd::logger::print_levels)"
  # _debug_escape_codes "$(ndd::logger::print_levels)"

  assertNotContains "${actual_message}"    "ansi"

  assertContains    "${actual_message}"    $'0 FATAL       \e[30m\e[1m\e[41m Some text as example \e[22;39;49m'
  assertContains    "${actual_message}"    $'1 ERROR       \e[31m\e[1m Some text as example \e[22;39m'
  assertContains    "${actual_message}"    $'2 WARNING     \e[33m\e[1m Some text as example \e[22;39m'
  assertContains    "${actual_message}"    $'3 INFORMATION \e[97m\e[1m Some text as example \e[22;39m'
  assertContains    "${actual_message}"    $'4 DEBUG        Some text as example '
  assertContains    "${actual_message}"    $'5 TRACE        Some text as example '

  # The old look and feel with too many colors:
  # assertContains    "${actual_message}"    $'0 FATAL       \e[30m\e[48;2;255;0;0m\e[1m Some text as example \e[22;39;49m'
  # assertContains    "${actual_message}"    $'1 ERROR       \e[38;2;255;0;0m Some text as example \e[39m'
  # assertContains    "${actual_message}"    $'2 WARNING     \e[38;2;255;127;0m Some text as example \e[39m'
  # assertContains    "${actual_message}"    $'3 INFORMATION \e[38;2;255;255;0m Some text as example \e[39m'
  # assertContains    "${actual_message}"    $'4 DEBUG       \e[38;2;0;255;0m Some text as example \e[39m'
  # assertContains    "${actual_message}"    $'5 TRACE       \e[38;2;0;128;255m Some text as example \e[39m'
}



# ------------------------------------------------------------------------------
export ANSI_FORCE_SUPPORT=true

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/shunit2/shunit2"
